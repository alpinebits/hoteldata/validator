#!/bin/bash

if [ -z "$1" ]; then
    exit 1
fi

# OTA: need to validate against all known root elements, if at least one validates, we're good

OTA_GOOD=0
for OTA in OTA_HotelAvailNotifRQ.xsd OTA_HotelAvailNotifRS.xsd OTA_HotelDescriptiveContentNotifRQ.xsd OTA_HotelDescriptiveContentNotifRS.xsd OTA_HotelRatePlanNotifRQ.xsd OTA_HotelRatePlanNotifRS.xsd OTA_NotifReportRQ.xsd OTA_NotifReportRS.xsd OTA_ReadRQ.xsd OTA_ResRetrieveRS.xsd ; do
    xmllint --noout --schema schemas-201507/schema-ota/$OTA "$1" 2> /dev/null
    if [ $? -eq 0 ]; then
        OTA_GOOD=1
        break
    fi
done
echo -n $OTA_GOOD

# AlpineBits XSD

xmllint --noout --schema schemas-201507/schema-xsd/alpinebits.xsd "$1" 2> /dev/null
if [ $? -eq 0 ]; then
    echo -n 1
else
    echo -n 0
fi

# AlpineBits RelaxNG

xmllint --noout --relaxng schemas-201507/schema-rng/alpinebits.rng "$1" 2> /dev/null
if [ $? -eq 0 ]; then
    echo -n 1
else
    echo -n 0
fi
