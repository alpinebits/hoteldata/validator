# AlpineBits HotelData validator backend

This repo contains the source code of the validator backend of https://development.alpinebits.org/#/validator .

You can use this if you wish to do local validations (instead of using the service at the above site).

You need to have xmllint installed, then you can call the script corresponding to
the right AlpineBits version with the XML file to validate as an argument.

For completeness, the webserver code written for Node.js is also available in this repo (srv.js).

## Example usage

```
$ git clone https://gitlab.com/alpinebits/hoteldata/validator.git
Cloning into 'validator'...
remote: Enumerating objects: 645, done.
remote: Counting objects: 100% (645/645), done.
remote: Compressing objects: 100% (94/94), done.
remote: Total 645 (delta 543), reused 636 (delta 538), pack-reused 0
Receiving objects: 100% (645/645), 1.20 MiB | 12.02 MiB/s, done.
Resolving deltas: 100% (543/543), done.

$ cd validator/

$ curl -L -o sample.xml https://gitlab.com/alpinebits/hoteldata/standard-specification/-/raw/master/files/samples/Activity-OTA_HotelPostEventNotifRQ-deletion.xml?inline=false
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   992  100   992    0     0   2480      0 --:--:-- --:--:-- --:--:--  2473

$ cat sample.xml 
<?xml version="1.0" encoding="UTF-8"?>
<OTA_HotelPostEventNotifRQ
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns="http://www.opentravel.org/OTA/2003/05"
        xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelPostEventNotifRQ.xsd"
        Version="8.000">

    <EventReports>
        <EventReport>
            <EventSites>
                <EventSite HotelCode="00001" HotelName="TestHotel" >
                    <Event_ID Type="18" ID="41654" ID_Context="providerNameSrl" />
                </EventSite>
            </EventSites>
            <GeneralEventInfo />
        </EventReport>
        <EventReport>
            <EventSites>
                <EventSite HotelCode="00001" HotelName="TestHotel" >
                    <Event_ID Type="18" ID="41655" ID_Context="providerNameSrl" />
                </EventSite>
            </EventSites>
            <GeneralEventInfo />
        </EventReport>
    </EventReports>
</OTA_HotelPostEventNotifRQ>chris@ganymede:~/validator$ 

$ ./validate-2020-10.sh sample.xml
111
```

The outcome are three bits, indicating the validation result for OTA, AlpineBits XSD and AlpineBits RNG, respectively.

