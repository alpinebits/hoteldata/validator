/*
        AlpineBits message validator

        https://development.alpinebits.org/#/validator

        (C) 2017-2022 AlpineBits Alliance - www.alpinebits.org

        change log:
        2.3.0 - 2022-11-10 chris@1006.org added support for 2022-10
        2.2.0 - 2020-09-03 chris@1006.org added support for 2020-10, incremented validation script timeout from 5 to 10 seconds
        2.1.0 - 2019-03-29 chris@1006.org added support for 2018-10
        2.0.0 - 2017-09-10 chris@1006.org

*/

"use strict";

const express = require("express");
const cors = require("cors");
const bodyparser = require("body-parser");

const fs = require("fs");
const execSync = require("child_process").execSync;
const crypto = require("crypto");

const UPLOAD_DIR = "uploads/";

const server = express();
server.use(cors());
server.use(bodyparser.json({limit: "10mb"}));


// a function to generate a random name

const rname = () => {
    const buf = crypto.randomBytes(10);
    const s = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let ret = "";
    let i;
    for (i = 0; i < 10; i++) {
       ret += s.charAt(buf[i] % 62);
    }
    return ret;
};


// we just handle a POST to /validator

server.post("/validator", function (req, res) {

    let ip;
    if (req && req.headers) {
        ip = res.req.headers['x-forwarded-for'];
    }

    // parse parameters

    if (req.body === undefined) {
        let msg = "ERROR (type=no body, status=500)";
        console.log(ip + ';' + (new Date()) + "; " + msg);
        res.status(500).send(msg + "\n");
        return;
    }

    let version = "";
    if (req.body.version !== undefined &&
        ["2014-04", "2015-07", "2015-07b", "2017-10", "2018-10", "2020-10", "2022-10"].includes(req.body.version)) {
        version = req.body.version;
    }

    let msg = "";
    if (req.body.msg !== undefined) {
        msg = req.body.msg;
    }

    if (version === "" || msg === "") {
        let msg = "ERROR (type=empty or invalid parameters, status=400)";
        console.log(ip + ';' + (new Date()) + "; " + msg);
        res.status(400).send(msg + "\n");
        return;
    }

    // store msg to file 

    let filename = UPLOAD_DIR + rname();
    fs.writeFileSync(filename, msg, {encoding: "UTF8"});

    // call validation script

    let stdout = String(execSync("./validate-" + version + ".sh " + filename, { timeout: 10000 }));

    // unlink file

    fs.unlinkSync(filename);

    // return validation results

    if (stdout === undefined || !(["000", "001", "010", "011", "100", "101", "110", "111"].includes(stdout)) ) {
        let msg = "ERROR (type=validation script failure, status=500)";
        console.log(ip + ';' + (new Date()) + "; " + msg);
        res.status(500).send(msg + "\n");
        return;
    }

    let ota = Boolean(Number(stdout.charAt(0)));
    let xsd = Boolean(Number(stdout.charAt(1)));
    let rng = Boolean(Number(stdout.charAt(2)));

    let data = { version: version, result: { ota: ota, xsd: xsd, rng: rng } };
    console.log(ip + ';' + (new Date()) + "; OK: " + JSON.stringify(data));
    res.send(data);  
    return;

});


// catch-all error handler

server.use( (err, req, res, next) => {
    let ip;
    if (req && req.headers) {
        ip = res.req.headers['x-forwarded-for'];
    }
    let type = "unknown";
    if (err.type) {
        type = err.type;
    }
    let status = 500;
    if (err.status) {
        status = err.status;
    }
    let msg = "ERROR (type=" + type + ", status=" + status + ")";
    console.log(ip + ';' + (new Date()) + "; " + msg);
    res.status(status).send(msg + "\n");
    return;
});


// start the server

server.listen(8081, "127.0.0.1");

